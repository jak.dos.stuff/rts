# Contributing

## Code of Conduct

This project and everyone participating in it is governed by the [Code of Conduct](CODE_OF_CONDUCT.md).By participating,
you are expected to uphold this code. Please report unacceptable behavior to the
[team](mailto:incoming+jak-dos-stuff-rts-21249148-issue-@incoming.gitlab.com).

## General Guidelines

* This is a project built mainly on Unity's ECS framework, with minimal concessions being made where absolutely necessary.
Rule of thumb: If it _can_ be ECS, it _should_ be ECS.
* All logic _**must**_ be tested, meaning all systems etc need to be tested, but not components.
* Every feature documented in the [TDD](https://gitlab.com/jak.dos.stuff/rts/-/wikis/tdd#features) must have at least
one playmode integration test to confirm that all interacting systems play nicely together with those already provided
by Unity
* Several templates have been created to make creating an issue easier. Please use them.
* Any issue tagged with the `blocked` or `hold` tags should have the dependency in the description or comments

### General Workflow

1) Grab an issue
    * Reach out to maintainer to begin dev work
2) Create branch from `master`
3) Work, work, work
    * If work takes more than a day or so, make sure to comment on the story with progress updates
4) REBASE!
    * Make sure to pull master and rebase your branch before creating a PR. This will help to identify any potential 
    conflicts as well as keep the history clean.
5) Create MR
    * MR must be attached to the story
6) YOU ROCK!
7) Do it again!

### A Word on Bugs

Bugs are tracked as [issues](https://gitlab.com/jak.dos.stuff/rts/-/issues/). Create an issue and provide the following
information by filling in the [bug template](/.gitlab/issue_templates/bug.md).

* Explain the problem and include additional details to help maintainers reproduce the problem:
* **Use a clear and descriptive title** for the issue to identify the problem.
* **Where possible/necessary include screenshots and animated GIFs** which show you following the described steps and 
clearly demonstrate the problem. If you use the keyboard while following the steps, you can use
[this tool](https://www.cockos.com/licecap/) to record GIFs.
* **If you're reporting that the game crashed**, include a crash report with a stack trace from the operating system.
On macOS, the crash report will be available in `Console.app` under
"Diagnostic and usage information" > "User diagnostic reports". Include the crash report in the issue in a
[code block](https://docs.gitlab.com/ee/user/markdown.html#code-spans-and-blocks), a file attachment, or put it in a
[gist](https://gist.github.com/) and provide link to that gist.
* **If the problem wasn't triggered by a specific action**, describe what you were doing before the problem happened
and share more information using the guidelines below.

Provide more context by answering these questions:

* **Did the problem start happening recently** (e.g. after updating to a new version) or was this always a problem?
* If the problem started happening recently, **can you reproduce the problem in an older version?** Do you know what's
the most recent version in which the problem doesn't happen?
* **Can you reliably reproduce the issue?** If not, provide details about how often the problem happens and under
which conditions it normally happens.

Include details about your configuration and environment:

* **Which version of the game are you using?**
* **What's the name and version of the OS you're using**?

## Special Thanks

Issue templates were inspired by [GitLab's](https://gitlab.com/gitlab-org/gitlab/-/tree/master/.gitlab/issue_templates).

Inspiration for this document was provided by [Atom](https://github.com/atom/atom/blob/master/CONTRIBUTING.md) and
[Safia Abdalla](https://opensource.com/life/16/3/contributor-guidelines-template-and-tips)
