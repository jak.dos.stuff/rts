### Release notes

<!--
Concise blurb that can be used to quickly summarize what changed,
preferably past tense so that it can simply be copy/pasted into release notes.
-->

### Problem to solve / Functionality to implement

<!-- What problem does this solve or what needed functionality is missing? -->

### User experience goal

<!--
What is the single user experience does this address?
For example, "Players should be able to select their own units"
-->

### Proposal

<!--
How are we going to implement this? This should be high level, but should give a clear idea to anyone who has never participated
in any design discussions.
-->

### Further details

<!-- Include use cases, benefits, goals, or any other details that will help us understand the idea better. -->

### Relevant Concerns

<!-- Any other relevant concerns, e.g. security, potential scope creep, research, blockers -->

### Documentation

<!--
Add all known Documentation Requirements in this section, e.g. changes to existing documents, new documents
*Note* Completed TA document(s) is a **REQUIRED** part of this portion before feature work can kick off
-->

### Availability & Testing

<!--
This section needs to be retained and filled in during the workflow planning breakdown phase of this feature proposal,
if not earlier.

What risks does this change pose to our availability? How might it affect the quality of the product? What additional
test coverage or changes to tests will be needed? Will it require cross-browser testing?

Please list the test areas (unit, integration and end-to-end) that needs to be added or updated to ensure that this feature
will work as intended. Please use the list below as guidance.
* Unit test changes
* Integration test changes
* End-to-end test change
-->

### Acceptance Criteria

<!--
What does success look like, and how can we measure that?
Define both the acceptance criteria to indicate when the solution is working correctly. If there is no way to measure success, 
link to an issue that will implement a way to measure this.
-->

### Related issues

<!-- Once written, link all related sub issues (story, documentation, refactors, test plans) here -->

/label ~"feature::epic" ~"status::ready" ~"epic::"
