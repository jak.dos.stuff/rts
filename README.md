# RTS

Unity portion of a currently unnamed RTS game. If you're interested in contributing, check out our
[guidelines](CONTRIBUTING.md). For more details check out the [wiki](https://gitlab.com/jak.dos.stuff/rts/-/wikis/home).

Currently supported version of Unity is 2020.2.0f1.
