﻿using Systems.Movement;
using Components;
using NUnit.Framework;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Tests.EditorTests.SystemTests
{
    [TestFixture]
    [Category("system")]
    [Category("targeting")]
    public class RotateUnitTests : BaseSystemTest
    {
        [SetUp]
        public void SetUp()
        {
            BaseSetup(
                new[] { typeof(RotateUnits) },
                new ComponentType[]
                {
                    typeof(Translation),
                    typeof(Rotation),
                    typeof(MovementStats)
                });
        }

        [Test]
        public void Given_EntityWithNoTarget_When_WorldUpdates_Then_ExpectNoChangeInRotation()
        {
            var entity = EntityManager.CreateEntity(Archetype);
            EntityManager.SetComponentData(entity, new MovementStats { MoveSpeed = 1.0F, TurnSpeed = 1.0F });

            World.SetUpdate();

            Assert.AreEqual(new Rotation(), EntityManager.GetComponentData<Rotation>(entity));
        }

        [Test]
        public void Given_EntityWithATarget_When_FrameProgresses_Then_AssertMovement()
        {
            var entity = EntityManager.CreateEntity(Archetype);
            EntityManager.AddComponentData(entity, new Target { Position = new float3(100.0F, 0.0F, 100.0F) });
            EntityManager.AddComponent<TargetAcquired>(entity);
            EntityManager.SetComponentData(entity, new MovementStats { MoveSpeed = 3.0F, TurnSpeed = 3.0F });

            World.SetUpdate();

            var expectedQuat = new quaternion(0.0F, -0.38F, 0.0F, -0.92F);
            var actualQuat = EntityManager.GetComponentData<Rotation>(entity).Value;

            Assert.AreEqual(
                expectedQuat.value.y,
                actualQuat.value.y,
                0.01F
            );
            Assert.AreEqual(
                expectedQuat.value.w,
                actualQuat.value.w,
                0.01F
            );
        }

        [Test]
        public void Given_EntityWithATarget_When_TargetChanges_Then_AssertMovement()
        {
            var entity = EntityManager.CreateEntity(Archetype);
            EntityManager.AddComponentData(entity, new Target { Position = new float3(100.0F, 0.0F, 100.0F) });
            EntityManager.AddComponent<TargetAcquired>(entity);
            EntityManager.SetComponentData(entity, new MovementStats { MoveSpeed = 3.0F, TurnSpeed = 3.0F });

            World.SetUpdate();

            var expectedRotation = new float4(0.0F, -0.38F, 0.0F, -0.92F);
            var actualRotation = EntityManager.GetComponentData<Rotation>(entity).Value.value;

            Assert.AreEqual(
                expectedRotation.y,
                actualRotation.y,
                0.01F
            );
            Assert.AreEqual(
                expectedRotation.w,
                actualRotation.w,
                0.01F
            );

            EntityManager.SetComponentData(
                entity,
                new Target { Position = new float3(-55.0F, 0.0F, 24.36F) }
            );

            World.SetUpdate();
            actualRotation = EntityManager.GetComponentData<Rotation>(entity).Value.value;

            Assert.True(math.abs(expectedRotation.y - actualRotation.y) > 0.0F);
            Assert.True(math.abs(expectedRotation.w - actualRotation.w) > 0.0F);

            expectedRotation = new float4(0.0F, 0.58F, 0.0F, 0.81F);

            Assert.AreEqual(
                expectedRotation.y,
                actualRotation.y,
                0.01F
            );
            Assert.AreEqual(
                expectedRotation.w,
                actualRotation.w,
                0.01F
            );
        }
    }
}