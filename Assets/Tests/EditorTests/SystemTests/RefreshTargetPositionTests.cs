using Systems.TargetOrchestration;
using Components;
using NUnit.Framework;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Tests.EditorTests.SystemTests
{
    [TestFixture]
    [Category("system")]
    [Category("targeting")]
    public class RefreshTargetPositionTests : BaseSystemTest
    {
        private static readonly float3 TARGET_POSITION = new float3(90.0F, 2.0F, 10.0F);

        private EntityArchetype targetEntityArchetype;

        [SetUp]
        public void Setup()
        {
            BaseSetup(
                new[] { typeof(RefreshTargetPositions) },
                new ComponentType[] { typeof(Target) }
            );

            targetEntityArchetype = EntityManager.CreateArchetype(typeof(Translation));
        }

        [Test]
        public void Given_EntityWithNoTarget_When_WorldUpdates_Then_ExpectNoTarget()
        {
            var testEntity = EntityManager.CreateEntity();
            World.SetUpdate();
            Assert.False(EntityManager.HasComponent<Target>(testEntity));
        }

        [Test]
        public void Given_EntityWithTarget_When_WorldUpdates_Then_ExpectNoChangeInTarget()
        {
            var targetEntity = EntityManager.CreateEntity(targetEntityArchetype);
            EntityManager.SetComponentData(
                targetEntity,
                new Translation { Value = TARGET_POSITION }
            );

            var testEntity = EntityManager.CreateEntity(Archetype);
            EntityManager.SetComponentData(
                testEntity,
                new Target { Entity = targetEntity, Position = TARGET_POSITION }
            );

            World.SetUpdate();

            Assert.AreEqual(
                new Target { Entity = targetEntity, Position = TARGET_POSITION },
                EntityManager.GetComponentData<Target>(testEntity));
        }

        // Target -> Changes -> Matches new
        [Test]
        public void Given_EntityWithTarget_When_TargetMoves_Then_ExpectChangesReflected()
        {
            var updatedPos = new float3(511.0F, 451.0F, 369.0F);

            var targetEntity = EntityManager.CreateEntity(targetEntityArchetype);
            EntityManager.SetComponentData(
                targetEntity,
                new Translation { Value = TARGET_POSITION }
            );

            var testEntity = EntityManager.CreateEntity(Archetype);
            EntityManager.SetComponentData(
                testEntity,
                new Target { Entity = targetEntity, Position = TARGET_POSITION }
            );

            EntityManager.SetComponentData(
                targetEntity,
                new Translation { Value = updatedPos }
            );

            World.SetUpdate();

            Assert.AreEqual(
                new Target { Entity = targetEntity, Position = updatedPos },
                EntityManager.GetComponentData<Target>(testEntity)
            );
        }
    }
}