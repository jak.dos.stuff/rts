using Systems.TargetOrchestration;
using Components;
using Components.AuthoringTags;
using NUnit.Framework;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Tests.EditorTests.SystemTests
{
    [TestFixture]
    [Category("system")]
    [Category("targeting")]
    public class UpdateTargetTests : BaseSystemTest
    {
        private static readonly float3 TARGET_POS_1 = new float3(90.0F, 2.0F, 10.0F);
        private static readonly float3 TARGET_POS_2 = new float3(13.0F, 7.0F, 4.0F);

        private static EntityArchetype targetArchetype;

        [SetUp]
        public void SetUp()
        {
            BaseSetup(
                new[] { typeof(UpdateTargets) },
                new ComponentType[] { typeof(Selected) });

            targetArchetype = EntityManager.CreateArchetype(
                typeof(Targeted),
                typeof(Translation),
                typeof(Selectable)
            );
        }

        [Test]
        public void Given_SelectedEntityWithNoTargetsAndNoTargetInWorld_When_WorldUpdates_Then_ExpectNoTargetOnEntity()
        {
            var entity = EntityManager.CreateEntity(Archetype);
            World.SetUpdate();
            Assert.False(EntityManager.HasComponent<Target>(entity));
        }

        [Test]
        public void Given_NoSelectedEntity_When_NoTargetPresent_Then_ExpectEntityToNotHaveTarget()
        {
            var entity = EntityManager.CreateEntity();
            World.SetUpdate();
            Assert.False(EntityManager.HasComponent<Target>(entity));
        }

        [Test]
        public void Given_NoSelectedEntity_When_TargetCreated_Then_ExpectEntityToNotHaveTarget()
        {
            var entity = EntityManager.CreateEntity();
            EntityManager.CreateEntity(targetArchetype);

            World.SetUpdate();
            Assert.False(EntityManager.HasComponent<Target>(entity));
        }

        [Test]
        public void Given_SelectedEntityWithNoTarget_When_TargetCreated_Then_ExpectTargetToMatch()
        {
            var entity = EntityManager.CreateEntity(Archetype);

            var targetEntity = EntityManager.CreateEntity(targetArchetype);
            EntityManager.SetComponentData(
                targetEntity,
                new Translation { Value = TARGET_POS_1 }
            );

            World.SetUpdate();

            var targetComp = EntityManager.GetComponentData<Target>(entity);
            Assert.NotNull(targetComp);
            Assert.AreEqual(TARGET_POS_1, targetComp.Position);
        }

        [Test]
        public void Given_SelectedEntityWithTarget_When_TargetChanges_Then_ExpectTargetToMatch()
        {
            var entity = EntityManager.CreateEntity(Archetype);
            EntityManager.AddComponentData(entity, new Target { Position = TARGET_POS_1 });

            var targetEntity = EntityManager.CreateEntity(targetArchetype);
            EntityManager.SetComponentData(
                targetEntity,
                new Translation { Value = TARGET_POS_2 }
            );

            World.SetUpdate();

            var targetComp = EntityManager.GetComponentData<Target>(entity);
            Assert.NotNull(targetComp);
            Assert.AreEqual(TARGET_POS_2, targetComp.Position);
        }
    }
}
