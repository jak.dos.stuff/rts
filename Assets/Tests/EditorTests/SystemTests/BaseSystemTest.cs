﻿using System;
using Unity.Entities;

namespace Tests.EditorTests.SystemTests
{
    public abstract class BaseSystemTest
    {
        protected TestWorld World;
        protected EntityManager EntityManager;
        protected EntityArchetype Archetype;

        protected void BaseSetup(Type[] systems, ComponentType[] archetypeComponentTypes)
        {
            World = new TestWorld(systems);
            EntityManager = World.EntityManager;
            Archetype = EntityManager.CreateArchetype(archetypeComponentTypes);
        }
    }
}