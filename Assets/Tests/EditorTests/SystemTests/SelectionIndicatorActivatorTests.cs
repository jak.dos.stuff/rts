using Systems.Selection;
using Components;
using Components.AuthoringTags;
using NUnit.Framework;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Tests.EditorTests.SystemTests
{
    [TestFixture]
    [Category("system")]
    [Category("selection")]
    public class SelectionIndicatorActivatorTests : BaseSystemTest
    {
        private static EntityArchetype indicatorArchetype;

        [SetUp]
        public void SetUp()
        {
            BaseSetup(
                new[] { typeof(SelectionIndicatorActivator) },
                new ComponentType[]
                {
                    typeof(Translation),
                    typeof(Rotation),
                    typeof(Selectable)
                });

            indicatorArchetype =
                EntityManager.CreateArchetype(
                    typeof(Translation),
                    typeof(Parent),
                    typeof(SelectionIndicator)
                );
        }

        [Test]
        public void Given_Unit_When_NotSelected_Then_ExpectIndicatorPositionToBeLowered()
        {
            var parent = EntityManager.CreateEntity(Archetype);
            var indicator = EntityManager.CreateEntity(indicatorArchetype);
            EntityManager.SetComponentData(indicator, new Parent { Value = parent });
            World.SetUpdate(2.0F);

            Assert.That(EntityManager.GetComponentData<Translation>(indicator).Value.y, Is.LessThanOrEqualTo(-1.25F));
        }

        [Test]
        public void Given_Unit_When_Selected_Then_ExpectIndicatorPositionToBeRaised()
        {
            var parent = EntityManager.CreateEntity(Archetype);
            EntityManager.AddComponent<Selected>(parent);

            var indicator = EntityManager.CreateEntity(indicatorArchetype);
            EntityManager.SetComponentData(indicator, new Parent { Value = parent });
            EntityManager.SetComponentData(
                indicator,
                new Translation { Value = new float3(0.0F, -1.25F, 0.0F) }
            );
            World.SetUpdate();

            Assert.That(EntityManager.GetComponentData<Translation>(indicator).Value.y, Is.GreaterThanOrEqualTo(-1.0F));
        }

        [Test]
        public void Given_Unit_When_SelectedAndThenUnSelected_Then_ExpectIndicatorPositionToReset()
        {
            var parent = EntityManager.CreateEntity(Archetype);
            EntityManager.AddComponent<Selected>(parent);

            var indicator = EntityManager.CreateEntity(indicatorArchetype);
            EntityManager.SetComponentData(indicator, new Parent { Value = parent });
            EntityManager.SetComponentData(
                indicator,
                new Translation { Value = new float3(0.0F, -1.0F, 0.0F) }
            );
            World.SetUpdate();

            EntityManager.RemoveComponent<Selected>(parent);
            World.SetUpdate(2.0F);

            Assert.That(EntityManager.GetComponentData<Translation>(indicator).Value.y, Is.LessThanOrEqualTo(-1.25F));
        }
    }
}
