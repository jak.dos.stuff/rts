using System;
using System.Collections.Generic;
using Unity.Core;
using Unity.Entities;

namespace Tests.EditorTests
{
    public class TestWorld : World
    {
        public TestWorld(IReadOnlyList<Type> testSystems) : base("test-world")
        {
            DefaultWorldInitialization.AddSystemsToRootLevelSystemGroups(this, testSystems);
        }
        
        public void SetUpdate(float elapsedSeconds = 1.0F)
        {
            SetTime(new TimeData(UnityEngine.Time.time, elapsedSeconds));
            Update();
        }
    }
}