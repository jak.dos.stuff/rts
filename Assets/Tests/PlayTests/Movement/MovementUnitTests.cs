using System.Collections;
using Components;
using NUnit.Framework;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.TestTools;
using Unity.Collections;
using JetBrains.Annotations;
using UnityEngine.SceneManagement;

namespace Tests.PlayTests.Movement
{
    [TestFixture]
    [Category("movement")]
    public class MovementUnitTests : BasePlayTest
    {
        private static EntityArchetype Archetype =
            EntityManager.CreateArchetype(
                new ComponentType[]
                {
                    typeof(Translation),
                    typeof(Rotation),
                    typeof(MovementStats)
                });

        [UnitySetUp]
        [UsedImplicitly]
        public IEnumerator SetUp()
        {
            yield return SceneManager.LoadSceneAsync("Units", new LoadSceneParameters(LoadSceneMode.Additive));
            SceneManager.SetActiveScene(SceneManager.GetSceneByName("MovementTestScene"));
        }

        [UnityTest]
        public IEnumerator Given_EntityWithNoTarget_When_FrameProgresses_Then_AssertNoMovement()
        {
            var entity = EntityManager.CreateEntity(Archetype);
            EntityManager.SetComponentData(entity, new MovementStats { MoveSpeed = 1.0F, TurnSpeed = 1.0F });

            yield return new WaitForSeconds(0.25F);

            Assert.AreEqual(new Translation(), EntityManager.GetComponentData<Translation>(entity));
        }

        [UnityTest]
        public IEnumerator Given_EntityWithATarget_When_FrameProgresses_Then_AssertMovement()
        {
            var enemyUnits = EnemyUnitQuery.ToEntityArray(Allocator.Temp);
            Assert.That(enemyUnits.Length, Is.EqualTo(1));

            var enemyEntity = enemyUnits[0];
            EntityManager.AddComponent<Targeted>(enemyEntity);
            var targetPosition = EntityManager.GetComponentData<Translation>(enemyEntity);

            var playerUnits = PlayerUnitQuery.ToEntityArray(Allocator.Temp);
            Assert.That(playerUnits.Length, Is.EqualTo(1));

            var playerEntity = playerUnits[0];
            Assert.False(EntityManager.HasComponent<Selected>(playerEntity));

            EntityManager.AddComponent<TargetAcquired>(playerEntity);
            EntityManager.AddComponentData(playerEntity, new Target { Entity = enemyEntity, Position = targetPosition.Value });

            yield return new WaitForSeconds(1.0F);

            var updatedTranlsation = EntityManager.GetComponentData<Translation>(playerEntity);

            Assert.That(updatedTranlsation.Value.x, Is.EqualTo(2.150F).Within(0.06F));
            Assert.That(updatedTranlsation.Value.z, Is.EqualTo(-1.398F).Within(0.07F));
        }
    }
}
