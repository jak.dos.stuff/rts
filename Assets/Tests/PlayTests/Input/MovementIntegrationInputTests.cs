﻿using System.Collections;
using Components;
using JetBrains.Annotations;
using NUnit.Framework;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace Tests.PlayTests.Input
{
    [Category("integration")]
    [Category("movement")]
    public class MovementIntegrationInputTests : BasePlayTest
    {
        // Expected rotations
        private static readonly Rotation INITIAL_ROTATION =
            new Rotation { Value = new quaternion(0.000F, 0.000F, 0.000F, 1.000F) };

        private static readonly Rotation CHANGED_ROTATION =
            new Rotation { Value = new quaternion(0.000F, 0.840F, 0.000F, 0.542F) };

        private static readonly Rotation FINAL_ROTATION =
            new Rotation { Value = new quaternion(0.000F, 0.841F, 0.000F, 0.541F) };

        [UnitySetUp]
        [UsedImplicitly]
        public IEnumerator SetUp()
        {
            yield return SceneManager.LoadSceneAsync("Units", new LoadSceneParameters(LoadSceneMode.Additive));
            SceneManager.SetActiveScene(SceneManager.GetSceneByName("MovementTestScene"));
        }

        [UnityTest]
        public IEnumerator Given_SceneWithUnits_When_PlayerUnitSelectedAndEnemyTargeted_Then_ExpectMovementTowardEnemy()
        {
            yield return selectPlayerUnit();
            yield return assertPlayerUnitInitialConfiguration();

            yield return targetEnemyUnit();
            yield return assertTargetValues();

            yield return assertUpdatedPositionInfo();

            yield return assertFinalPositionInfo();
        }

        private IEnumerator assertPlayerUnitInitialConfiguration()
        {
            EntityManager.SetComponentData(
                PlayerEntity,
                new MovementStats { MoveSpeed = 3.0F, TurnSpeed = 7.0F }
            );
            yield return null;

            assertExpectedRotation(INITIAL_ROTATION, EntityManager.GetComponentData<Rotation>(PlayerEntity));
        }

        private IEnumerator assertUpdatedPositionInfo()
        {
            // Allow playerUnit to move towards the target 
            yield return new WaitForSeconds(1.5F);

            Assert.That(
                EntityManager.GetComponentData<Translation>(PlayerEntity),
                Is.Not.EqualTo(OriginalPlayerPosition)
            );

            assertExpectedRotation(CHANGED_ROTATION, EntityManager.GetComponentData<Rotation>(PlayerEntity));
        }

        private IEnumerator assertFinalPositionInfo()
        {
            yield return new WaitForSeconds(3.0F);

            Assert.False(EntityManager.HasComponent<TargetAcquired>(PlayerEntity));

            var stoppedPosition = EntityManager.GetComponentData<Translation>(PlayerEntity);

            yield return new WaitForSeconds(0.5F);

            var finalPosition = EntityManager.GetComponentData<Translation>(PlayerEntity);
            Assert.That(finalPosition.Value.x, Is.EqualTo(stoppedPosition.Value.x).Within(0.005F));
            Assert.That(finalPosition.Value.y, Is.EqualTo(stoppedPosition.Value.y).Within(0.005F));
            Assert.That(finalPosition.Value.z, Is.EqualTo(stoppedPosition.Value.z).Within(0.005F));

            assertExpectedRotation(FINAL_ROTATION, EntityManager.GetComponentData<Rotation>(PlayerEntity));
        }

        private static void assertExpectedRotation(Rotation expectedRotation, Rotation actualRotation)
        {
            var expected = expectedRotation.Value.value;
            var actual = actualRotation.Value.value;

            Assert.That(actual.x, Is.EqualTo(expected.x).Within(0.005F));
            Assert.That(actual.y, Is.EqualTo(expected.y).Within(0.005F));
            Assert.That(actual.z, Is.EqualTo(expected.z).Within(0.005F));
            Assert.That(actual.w, Is.EqualTo(expected.w).Within(0.005F));
        }
    }
}
