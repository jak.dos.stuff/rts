using System.Collections;
using System.Linq;
using Components;
using Components.AuthoringTags;
using JetBrains.Annotations;
using NUnit.Framework;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.TestTools;

namespace Tests.PlayTests.Input
{
    [Category("multiSelection")]
    public class MultiSelectionInputTests : BasePlayTest
    {
        private static EntityQuery MultiSelectorQuery =>
            EntityManager.CreateEntityQuery(typeof(MultiSelector));

        private static EntityQuery SelectedUnitQuery =>
            EntityManager.CreateEntityQuery(typeof(Selected), ComponentType.Exclude<EnemyUnit>());

        [UnitySetUp]
        [UsedImplicitly]
        public IEnumerator SetUp()
        {
            yield return SetUpUnits(2);
        }

        [UnityTest]
        public IEnumerator Given_FreshScene_Then_ExpectNoMultiSelectorPresent()
        {
            Assert.True(MultiSelectorQuery.IsEmpty);
            yield return null;
        }

        [UnityTest]
        public IEnumerator Given_AScene_When_MultiSelectActionTriggered_Then_ExpectMultiSelectorToBePresent()
        {
            Assert.True(MultiSelectorQuery.IsEmpty);
            yield return selectPlayerUnit(2);
            Input.Press(Mouse.leftButton);

            yield return new WaitForSeconds(1.0F);

            var selectors = MultiSelectorQuery.ToEntityArray(Allocator.Temp);
            Assert.That(selectors.Length, Is.EqualTo(1));

            Input.Release(Mouse.leftButton);
            Assert.True(MultiSelectorQuery.IsEmpty);
        }

        [UnityTest]
        public IEnumerator Given_AScene_When_MultiSelectActionTriggeredOverMultipleUnits_Then_ExpectUnitsToBeSelected()
        {
            var playerUnits = PlayerUnitQuery.ToEntityArray(Allocator.Temp);
            Assert.That(playerUnits.Length, Is.EqualTo(2));
            Assert.False(playerUnits.Any(unit => EntityManager.HasComponent<Selected>(unit)));

            var unit1 = playerUnits[0];
            var unit2 = playerUnits[1];

            var unit1Position = EntityManager.GetComponentData<Translation>(unit1);
            Vector2 unit1ScreenPos = InputListener.camera.WorldToScreenPoint(unit1Position.Value);
            InputState.Change(Mouse.position, unit1ScreenPos);
            yield return null;

            Input.Press(Mouse.leftButton);
            yield return new WaitForSeconds(0.25F);

            var unit2Position = EntityManager.GetComponentData<Translation>(unit2);
            Vector2 unit2ScreenPos = InputListener.camera.WorldToScreenPoint(unit2Position.Value);
            InputState.Change(Mouse.position, unit2ScreenPos);
            yield return null;

            Input.Release(Mouse.leftButton);
            yield return null;

            playerUnits = PlayerUnitQuery.ToEntityArray(Allocator.Temp);
            var selectedUnits = SelectedUnitQuery.ToEntityArray(Allocator.Temp);
            Assert.That(selectedUnits.Length, Is.EqualTo(2));
            Assert.True(playerUnits.All(unit => selectedUnits.Contains(unit)));
        }
    }
}
