using System.Collections;
using System.Linq;
using Components;
using Components.AuthoringTags;
using JetBrains.Annotations;
using NUnit.Framework;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.TestTools;

namespace Tests.PlayTests.Input
{
    [Category("singleSelection")]
    public class SingleSelectionInputTests : BasePlayTest
    {
        private static EntityQuery SelectedUnitQuery =>
            EntityManager.CreateEntityQuery(typeof(Selected), ComponentType.Exclude<EnemyUnit>());

        private static EntityQuery SelectionIndicatorQuery =>
            EntityManager.CreateEntityQuery(typeof(SelectionIndicator));

        [UnitySetUp]
        [UsedImplicitly]
        public IEnumerator SetUp()
        {
            yield return SetUpUnits(1);
        }

        [UnityTest]
        public IEnumerator Given_NothingIsSelected_When_InputIsReceived_Then_ExpectNothingToBeSelected()
        {
            Assert.False(PlayerUnitQuery.IsEmpty);
            Assert.True(SelectedUnitQuery.IsEmpty);
            Input.PressAndRelease(Mouse.leftButton);
            yield return null;
            Assert.True(SelectedUnitQuery.IsEmpty);
        }

        [UnityTest]
        public IEnumerator Given_NothingIsSelected_When_SelectIsReceived_Then_ExpectUnitToBeSelected()
        {
            Assert.True(SelectedUnitQuery.IsEmpty);

            yield return selectPlayerUnit();
            yield return new WaitForSeconds(2.0F);

            var selectedUnits = SelectedUnitQuery.ToEntityArray(Allocator.Temp);
            Assert.That(selectedUnits.Length, Is.EqualTo(1));
            Assert.True(EntityManager.HasComponent<Selected>(PlayerEntity));

            var selectionIndicator = findSelectionIndicatorEntityFromParent(PlayerEntity);
            var indicatorHeight = EntityManager.GetComponentData<Translation>(selectionIndicator).Value.y;
            Assert.That(indicatorHeight, Is.GreaterThanOrEqualTo(-1.0F));
        }

        [UnityTest]
        public IEnumerator Given_UnitIsSelected_When_SelectNothingIsReceived_Then_ExpectNothingToBeSelected()
        {
            yield return selectPlayerUnit();

            var selectedUnits = SelectedUnitQuery.ToEntityArray(Allocator.Temp);
            Assert.That(selectedUnits.Length, Is.EqualTo(1));
            Assert.True(EntityManager.HasComponent<Selected>(PlayerEntity));

            Vector2 someOtherPosition = InputListener.camera.WorldToScreenPoint(OriginalPlayerPosition.Value - 5);
            InputState.Change(Mouse.position, someOtherPosition);
            yield return null;

            Input.PressAndRelease(Mouse.leftButton);
            yield return new WaitForSeconds(2.0F);

            selectedUnits = SelectedUnitQuery.ToEntityArray(Allocator.Temp);
            Assert.That(selectedUnits.Length, Is.EqualTo(0));
            Assert.False(EntityManager.HasComponent<Selected>(PlayerEntity));

            var selectionIndicator = findSelectionIndicatorEntityFromParent(PlayerEntity);
            var indicatorHeight = EntityManager.GetComponentData<Translation>(selectionIndicator).Value.y;
            Assert.That(indicatorHeight, Is.LessThanOrEqualTo(-1.25F));
        }

        private static Entity findSelectionIndicatorEntityFromParent(Entity parent)
        {
            var selectionIndicators = SelectionIndicatorQuery.ToEntityArray(Allocator.Temp);
            return selectionIndicators.First(
                selectionIndicator =>
                    EntityManager.GetComponentData<Parent>(selectionIndicator).Value.Index == parent.Index
            );
        }
    }
}
