using System.Collections;
using Components;
using Components.AuthoringTags;
using JetBrains.Annotations;
using NUnit.Framework;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.TestTools;

namespace Tests.PlayTests.Input
{
    [Category("targeting")]
    public class TargetingInputTests : BasePlayTest
    {
        private static EntityQuery TargetedUnitQuery => EntityManager.CreateEntityQuery(typeof(Targeted));

        [UnitySetUp]
        [UsedImplicitly]
        public IEnumerator SetUp()
        {
            yield return SetUpUnits(1, 1);
        }

        [UnityTest]
        public IEnumerator Given_NoUnitsSelected_When_TargetTriggered_Then_ExpectNothingToBeTargeted()
        {
            Assert.False(EnemyUnitQuery.IsEmpty);
            Assert.True(TargetedUnitQuery.IsEmpty);
            Input.PressAndRelease(Mouse.rightButton);
            yield return null;
            Assert.True(TargetedUnitQuery.IsEmpty);
        }

        [UnityTest]
        public IEnumerator Given_PlayerUnitSelected_When_EnemyTargeted_Then_ExpectEnemyToBeTargeted()
        {
            yield return selectPlayerUnit();
            yield return targetEnemyUnit();
            yield return assertTargetValues();

            var targetedUnits = TargetedUnitQuery.ToEntityArray(Allocator.Temp);
            Assert.True(targetedUnits.Length == 1);
        }

        [UnityTest]
        public IEnumerator Given_EnemyTargeted_When_SelectedUnitChanges_Then_ExpectEnemyToNotBeTargeted()
        {
            yield return selectPlayerUnit();
            yield return targetEnemyUnit();
            yield return assertTargetValues();

            Input.PressAndRelease(Mouse.leftButton);
            yield return null;

            var targetedUnits = TargetedUnitQuery.ToEntityArray(Allocator.Temp);
            Assert.True(targetedUnits.Length == 0);
        }

        [UnityTest]
        public IEnumerator Given_EnemyUnitSelected_When_TargetActionTriggered_Then_ExpectNoTargets()
        {
            var selectedEnemy = EntityManager.CreateEntity(
                typeof(Unit),
                typeof(EnemyUnit),
                typeof(Selectable),
                typeof(Selected)
            );

            var enemies =
                EntityManager
                    .CreateEntityQuery(typeof(EnemyUnit), ComponentType.Exclude<Selected>())
                    .ToEntityArray(Allocator.Temp);

            Assert.That(enemies.Length, Is.EqualTo(1));

            TargetEntity = enemies[0];
            Assert.False(EntityManager.HasComponent<Targeted>(TargetEntity));

            TargetPos = EntityManager.GetComponentData<Translation>(TargetEntity);
            Vector2 targetUnitScreenPos = InputListener.camera.WorldToScreenPoint(TargetPos.Value);
            InputState.Change(Mouse.position, targetUnitScreenPos);
            yield return null;

            Input.PressAndRelease(Mouse.rightButton);
            yield return null;

            Assert.True(TargetedUnitQuery.IsEmpty);
            EntityManager.DestroyEntity(selectedEnemy);
            yield return null;
        }
    }
}
