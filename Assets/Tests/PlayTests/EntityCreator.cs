using System.Collections;
using UnityEngine;
using Random = System.Random;

namespace Tests.PlayTests
{
    public class EntityCreator : MonoBehaviour
    {
        private const float FLOAT_ZERO = 0.0F;

        private static readonly Random RANDOM = new Random();

        public GameObject playerPrefab;
        public GameObject enemyPrefab;

        internal uint PlayerCount = 0;
        internal uint EnemyCount = 0;

        public IEnumerator createUnits()
        {
            for (var i = 0; i < PlayerCount; i++)
            {
                instantiateUnit(playerPrefab);
            }

            yield return null;

            for (var i = 0; i < EnemyCount; i++)
            {
                instantiateUnit(enemyPrefab);
            }

            yield return null;
        }

        private static void instantiateUnit(GameObject prefab)
        {
            var position = new Vector3(RANDOM.Next(100), FLOAT_ZERO, RANDOM.Next(100));
            Instantiate(prefab, position, Quaternion.identity);
        }
    }
}
