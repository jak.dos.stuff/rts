using System.Collections;
using Components;
using Components.AuthoringTags;
using JetBrains.Annotations;
using NUnit.Framework;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace Tests.PlayTests
{
    [TestFixture]
    [Category("input")]
    public abstract class BasePlayTest
    {
        protected static EntityManager EntityManager => World.DefaultGameObjectInjectionWorld.EntityManager;

        protected static EntityQuery PlayerUnitQuery =>
            EntityManager.CreateEntityQuery(typeof(PlayerUnit));

        protected static EntityQuery EnemyUnitQuery =>
            EntityManager.CreateEntityQuery(typeof(EnemyUnit));

        private static EntityQuery AllUnitQuery =>
            EntityManager.CreateEntityQuery(typeof(Unit));

        protected readonly InputTestFixture Input = new InputTestFixture();
        protected readonly Mouse Mouse = InputSystem.AddDevice<Mouse>();

        protected PlayerInput InputListener;

        protected Entity PlayerEntity;
        protected Translation OriginalPlayerPosition;

        protected Entity TargetEntity;
        protected Translation TargetPos;

        [UnitySetUp]
        [UsedImplicitly]
        protected IEnumerator SetUpLevel()
        {
            yield return SceneManager.LoadSceneAsync("MovementTestScene");

            InputListener = GameObject.Find("InputListener").GetComponent<PlayerInput>();
        }

        protected static IEnumerator SetUpUnits(uint playerUnitCount = 0, uint enemyUnitCount = 0)
        {
            var entityCreator = GameObject.Find("UnitCreator").GetComponent<EntityCreator>();
            entityCreator.PlayerCount = playerUnitCount;
            entityCreator.EnemyCount = enemyUnitCount;
            yield return entityCreator.createUnits();
        }

        [TearDown]
        public void TearDown()
        {
            foreach (var unit in AllUnitQuery.ToEntityArray(Allocator.Temp))
            {
                EntityManager.DestroyEntity(unit);
            }
        }

        protected IEnumerator selectPlayerUnit(uint totalPlayerUnits = 1)
        {
            var playerUnits = PlayerUnitQuery.ToEntityArray(Allocator.Temp);
            Assert.That(playerUnits.Length, Is.EqualTo(totalPlayerUnits));

            PlayerEntity = playerUnits[0];
            Assert.False(EntityManager.HasComponent<Selected>(PlayerEntity));
            Assert.False(EntityManager.HasComponent<TargetAcquired>(PlayerEntity));

            OriginalPlayerPosition = EntityManager.GetComponentData<Translation>(PlayerEntity);

            Vector2 playerUnitScreenPos = InputListener.camera.WorldToScreenPoint(OriginalPlayerPosition.Value);
            InputState.Change(Mouse.position, playerUnitScreenPos);
            yield return null;

            // TODO: For now just making this work, but there is a flaw in PlayerController_Selection
            //  that will need to be addressed. Once it is, this should switch back to PressAndRelease.
            Input.Press(Mouse.leftButton);
            yield return new WaitForSeconds(0.25F);
            Input.Release(Mouse.leftButton);
            yield return new WaitForSeconds(0.25F);

            EntityManager.GetComponentTypes(PlayerEntity, Allocator.Temp).ToArray();

            Assert.True(EntityManager.HasComponent<Selected>(PlayerEntity));
        }

        protected IEnumerator targetEnemyUnit()
        {
            var enemyUnits = EnemyUnitQuery.ToEntityArray(Allocator.Temp);
            Assert.That(enemyUnits.Length, Is.EqualTo(1));

            TargetEntity = enemyUnits[0];
            Assert.False(EntityManager.HasComponent<Targeted>(TargetEntity));
            yield return null;

            TargetPos = EntityManager.GetComponentData<Translation>(TargetEntity);
            Vector2 targetUnitScreenPos = InputListener.camera.WorldToScreenPoint(TargetPos.Value);
            InputState.Change(Mouse.position, targetUnitScreenPos);
            yield return null;

            Input.PressAndRelease(Mouse.rightButton);
            yield return null;
        }

        protected IEnumerator assertTargetValues()
        {
            Assert.True(EntityManager.HasComponent<Targeted>(TargetEntity));

            Assert.True(EntityManager.HasComponent<TargetAcquired>(PlayerEntity));
            Assert.True(EntityManager.HasComponent<Target>(PlayerEntity));

            var playerTargetComp = EntityManager.GetComponentData<Target>(PlayerEntity);
            Assert.That(playerTargetComp.Entity, Is.EqualTo(TargetEntity));
            Assert.That(playerTargetComp.Position, Is.EqualTo(TargetPos.Value));

            yield return null;
        }
    }
}
