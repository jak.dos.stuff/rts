using Components;
using Components.AuthoringTags;
using Unity.Entities;
using Unity.Transforms;

namespace Systems.TargetOrchestration
{
    public class UpdateTargets : SystemBase
    {
        protected override void OnUpdate()
        {
            // TODO: This only really needs to fire once when a new target is selected. Does it really need a system?
            Entities.WithoutBurst()
                .WithStructuralChanges()
                .WithAll<Targeted, Translation>()
                .ForEach((Entity targetEntity) =>
                {
                    Entities
                        .WithoutBurst()
                        .WithStructuralChanges()
                        .WithAll<Selected>()
                        .WithNone<EnemyUnit>()
                        .ForEach((in Entity self) =>
                        {
                            if (targetEntity != self) // TODO: I don't think I have both branches of this tested
                            {
                                var target = EntityManager.GetComponentData<Translation>(targetEntity);
                                EntityManager.AddComponentData(
                                    self,
                                    new Target { Entity = targetEntity, Position = target.Value });
                                EntityManager.AddComponent<TargetAcquired>(self);
                            }
                        }).Run();
                }).Run();
        }
    }
}
