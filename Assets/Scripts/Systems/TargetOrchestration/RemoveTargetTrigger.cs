using Components;
using Components.AuthoringTags;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;

namespace Systems.TargetOrchestration
{
    // TODO: Can this class be refactored to use a distance query instead of this trigger?
    // See: https://docs.unity3d.com/Packages/com.unity.physics@0.5/manual/collision_queries.html#distance-query
    // Story: https://gitlab.com/jak.dos.stuff/rts/-/issues/15
    /// <summary>
    ///     Fires whenever a <see cref="Components.AuthoringTags.Unit"/> reaches their target
    /// </summary>
    public class RemoveTargetTrigger : JobComponentSystem
    {
        private BuildPhysicsWorld buildPhysicsWorld;
        private StepPhysicsWorld stepPhysicsWorld;
        private EndSimulationEntityCommandBufferSystem commandBufferSystem;

        protected override void OnCreate()
        {
            buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
            stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
            commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var triggerJob = new RemoveTargetTriggerJob
            {
                AllUnits = GetComponentDataFromEntity<Unit>(),
                AllTargets = GetComponentDataFromEntity<Target>(),
                EntityCommandBuffer = commandBufferSystem.CreateCommandBuffer()
            };

            var triggerHandle =
                triggerJob.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, inputDeps);
            commandBufferSystem.AddJobHandleForProducer(triggerHandle);
            return triggerHandle;
        }

        private struct RemoveTargetTriggerJob : ITriggerEventsJob
        {
            [ReadOnly] public ComponentDataFromEntity<Unit> AllUnits;
            [ReadOnly] public ComponentDataFromEntity<Target> AllTargets;

            public EntityCommandBuffer EntityCommandBuffer;

            public void Execute(TriggerEvent triggerEvent)
            {
                var entityA = triggerEvent.EntityA;
                var entityB = triggerEvent.EntityB;

                // TODO: Define physics layers to avoid colliding with literally everything...?
                if (!(AllUnits.HasComponent(entityA) && AllUnits.HasComponent(entityB))) return;

                // TODO: This will likely need to be adjusted if we allow for a unit to target (say to defend) another
                //  moving target. As it stands now, the moving target would just be reached and then abandoned, but
                //  in the case of defensive assignments, the target must continue to be the same.
                //  This would be handled by using a distance query in the movement system instead of a trigger like
                //  what I've done here.
                if (AllTargets.HasComponent(entityA) && AllTargets[entityA].Entity == entityB)
                {
                    EntityCommandBuffer.RemoveComponent<TargetAcquired>(entityA);
                }
                else if (AllTargets.HasComponent(entityB) && AllTargets[entityB].Entity == entityA)
                {
                    EntityCommandBuffer.RemoveComponent<TargetAcquired>(entityB);
                }
            }
        }
    }
}
