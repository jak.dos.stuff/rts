using Components;
using Unity.Entities;
using Unity.Transforms;

namespace Systems.TargetOrchestration
{
    /// <summary>
    ///     Ensures that <see cref="Components.AuthoringTags.Unit"/>'s are always moving towards
    ///     their <see cref="Target"/>
    /// </summary>
    public class RefreshTargetPositions : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithoutBurst()
                .ForEach((ref Target target) =>
                {
                    var targetPos = EntityManager.GetComponentData<Translation>(target.Entity);
                    target.Position = targetPos.Value;
                }).Run();
        }
    }
}
