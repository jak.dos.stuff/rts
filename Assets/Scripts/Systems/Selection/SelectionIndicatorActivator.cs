using Components;
using Components.AuthoringTags;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Systems.Selection
{
    public class SelectionIndicatorActivator : SystemBase
    {
        private const float MOVE_SPEED = 1.0F;
        private const float SELECTED_HEIGHT = -1.0F;
        private const float UNSELECTED_HEIGHT = -1.25F;

        protected override void OnUpdate()
        {
            var deltaTime = Time.DeltaTime;

            Entities
                .WithoutBurst()
                .WithAll<SelectionIndicator>()
                .ForEach((ref Translation position, in Parent parent) =>
                {
                    var parentIsSelected = EntityManager.HasComponent<Selected>(parent.Value);

                    switch (parentIsSelected)
                    {
                        case true when position.Value.y <= SELECTED_HEIGHT:
                            position.Value += math.up() * MOVE_SPEED * deltaTime;
                            break;
                        case false when position.Value.y >= UNSELECTED_HEIGHT:
                            position.Value += math.down() * MOVE_SPEED * deltaTime;
                            break;
                    }
                }).Run();
        }
    }
}
