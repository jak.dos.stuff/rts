using Components;
using JetBrains.Annotations;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.InputSystem;
using RaycastHit = Unity.Physics.RaycastHit;

namespace Systems.Selection
{
    [UsedImplicitly]
    public class ResizeMultiSelector : SystemBase
    {
        private static PhysicsWorld PhysicsWorld =>
            World.DefaultGameObjectInjectionWorld.GetExistingSystem<BuildPhysicsWorld>().PhysicsWorld;

        private Camera mainCamera;

        protected override void OnStartRunning()
        {
            mainCamera = Camera.main;
        }

        protected override void OnUpdate()
        {
            if (capturedNewMousePosition(out var hit))
            {
                Entities.ForEach((
                    ref MultiSelector multiSelector,
                    ref CompositeScale scale,
                    ref Translation position) =>
                {
                    // Typically invoking physics like this for every entity would be a really
                    // bad idea for performance, but in this case there should only ever be a
                    // single multi-selector in the game, so it's fine.
                    var mousePos = hit.Position;
                    var newDiagonal = new float3(mousePos.x, 0.0F, mousePos.z);

                    var newCenter = (newDiagonal + multiSelector.OriginPoint) / 2.0F;
                    var newScale = calcNewScale(newDiagonal, multiSelector.OriginPoint);

                    scale.Value = newScale;
                    position.Value = newCenter;
                }).Schedule();
            }
        }

        private bool capturedNewMousePosition(out RaycastHit hit)
        {
            var pointerPosition = Pointer.current.position.ReadValue();
            var positionRay = mainCamera.ScreenPointToRay(pointerPosition);

            var raycastInput = new RaycastInput
            {
                Start = positionRay.origin,
                End = positionRay.GetPoint(1000.0F),
                Filter = CollisionFilter.Default // TODO: Look more into this for a "targetable" filter
            };

            return PhysicsWorld.CastRay(raycastInput, out hit);
        }

        private static float4x4 calcNewScale(float3 newDiagonal, float3 originPoint)
        {
            var newScale = math.abs(newDiagonal - originPoint) * 0.1F;
            newScale.y = 0.1F;
            // applyMinimumScaleValues(ref newScale);
            return float4x4.Scale(newScale);
        }

        private static void applyMinimumScaleValues(ref float3 newScale)
        {
            if (newScale.x < 0.1F) newScale.x = 0.1F;
            if (newScale.y < 0.1F) newScale.y = 0.1F;
            if (newScale.z < 0.1F) newScale.z = 0.1F;
        }
    }
}
