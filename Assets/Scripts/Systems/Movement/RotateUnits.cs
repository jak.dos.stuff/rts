using Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Systems.Movement
{
    /// <summary>
    ///     Controls rotating <see cref="Components.AuthoringTags.Unit"/>s towards their current <see cref="Target"/>
    /// </summary>
    public class RotateUnits : SystemBase
    {
        protected override void OnUpdate()
        {
            var elapsedTime = Time.DeltaTime;

            Entities
                .WithAll<TargetAcquired>()
                .ForEach((
                    ref Rotation rotation,
                    in MovementStats movementStats,
                    in Translation position,
                    in Target target
                ) =>
                {
                    var slerpFactor = movementStats.TurnSpeed * elapsedTime;
                    var targetDirection = target.Position - position.Value;
                    var targetRotation = quaternion.LookRotationSafe(targetDirection, math.up());

                    rotation.Value = math.slerp(rotation.Value, targetRotation, slerpFactor);
                }).ScheduleParallel();
        }
    }
}
