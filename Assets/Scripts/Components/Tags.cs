using Unity.Entities;

namespace Components
{
    // TODO: I'll need a way to make sure that this can be removed if the target is destroyed before it is reached
    /// <summary>
    ///     Denotes an <see cref="Entity" >entity</see> actively has a target
    /// </summary>
    public struct TargetAcquired : IComponentData
    {
    }

    /// <summary>
    ///     Denotes an <see cref="Entity" >entity</see> is selected
    /// </summary>
    public struct Selected : IComponentData
    {
    }

    /// <summary>
    ///     Denotes an <see cref="Entity" >entity</see> is the active target
    /// </summary>
    public struct Targeted : IComponentData
    {
    }
}
