using JetBrains.Annotations;
using Unity.Entities;
using Unity.Mathematics;

namespace Components
{
    [GenerateAuthoringComponent]
    [UsedImplicitly]
    public struct MultiSelector : IComponentData
    {
        public float3 OriginPoint;
    }
}
