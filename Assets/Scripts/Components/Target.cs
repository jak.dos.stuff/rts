using Unity.Entities;
using Unity.Mathematics;

namespace Components
{
    // TODO: Should/Cound this be a shared component?
    /// <summary>
    ///     Contains details pertaining to an <see cref="Entity" >entity's</see> current target
    /// </summary>
    public struct Target : IComponentData
    {
        /// <value>The <see cref="Entity"/> represented by the <c>Target</c></value>
        public Entity Entity;

        /// <value>The <c>Target's</c> current position in the game world</value>
        public float3 Position;
    }
}
