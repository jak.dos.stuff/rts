using Unity.Entities;

namespace Components
{
    /// <summary>
    ///     Contains details affecting an <see cref="Entity" >entity's</see> movement
    /// </summary>
    [GenerateAuthoringComponent]
    public struct MovementStats : IComponentData
    {
        /// <summary>
        ///     How quickly the entity can turn
        /// </summary>
        public float TurnSpeed;

        /// <summary>
        ///     How quickly the entity moves through the game world
        /// </summary>
        public float MoveSpeed;

        /// <summary>
        ///     The maximum range at which the unit can engage a target in combat
        /// </summary>
        public int EngageRange;

        /// <summary>
        ///     The maximum range a target can move before the unit pursues. <br/>
        ///     This should always be larger than <see cref="engageRange"> <c>engageRange</c> </see>
        ///     in order to allow for an attack to happen before pursuit begins.
        /// </summary>
        public int PursueRange;
    }
}
