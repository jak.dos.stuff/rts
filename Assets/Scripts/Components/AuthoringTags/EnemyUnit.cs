using Unity.Entities;

namespace Components.AuthoringTags
{
    /// <summary>
    ///     Denotes an <see cref="Entity" >entity</see> is a unit opposing the player
    /// </summary>
    [GenerateAuthoringComponent]
    public struct EnemyUnit : IComponentData
    {
    }
}
