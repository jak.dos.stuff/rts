using Unity.Entities;

namespace Components.AuthoringTags
{
    /// <summary>
    ///     Denotes an <see cref="Entity" >entity</see> is a unit
    /// </summary>
    [GenerateAuthoringComponent]
    public struct Unit : IComponentData
    {
    }
}
