using Unity.Entities;

namespace Components.AuthoringTags
{
    /// <summary>
    ///     Denotes an <see cref="Entity" >entity</see> can be selected
    /// </summary>
    [GenerateAuthoringComponent]
    public struct Selectable : IComponentData
    {
    }
}
