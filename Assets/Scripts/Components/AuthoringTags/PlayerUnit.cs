using Unity.Entities;

namespace Components.AuthoringTags
{
    /// <summary>
    ///     Denotes an <see cref="Entity" >entity</see> is a unit controller by the player
    /// </summary>
    [GenerateAuthoringComponent]
    public struct PlayerUnit : IComponentData
    {
    }
}
