﻿using Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

using RaycastHit = Unity.Physics.RaycastHit;

namespace MonoBehaviours.Movmement
{
    public class MoveUnit : MonoBehaviour
    {
        private static EntityManager EntityManager => World.DefaultGameObjectInjectionWorld.EntityManager;
        private static EntityQuery MovingEntityQuery =>
            EntityManager.CreateEntityQuery(
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<MovementStats>(),
                ComponentType.ReadOnly<Rotation>(),
                ComponentType.ReadOnly<Target>());

        private void Update()
        {
            if (MovingEntityQuery.CalculateEntityCount() > 0)
            {
                ref PhysicsWorld world = ref World.DefaultGameObjectInjectionWorld.GetExistingSystem<BuildPhysicsWorld>().PhysicsWorld;

                var raycastHits = new NativeList<RaycastHit>(Allocator.TempJob);
                var entities = MovingEntityQuery.ToEntityArray(Allocator.TempJob);
                var translations = MovingEntityQuery.ToComponentDataArray<Translation>(Allocator.TempJob);
                var movementStats = MovingEntityQuery.ToComponentDataArray<MovementStats>(Allocator.TempJob);
                var rotations = MovingEntityQuery.ToComponentDataArray<Rotation>(Allocator.TempJob);
                var targets = MovingEntityQuery.ToComponentDataArray<Target>(Allocator.TempJob);

                var job = new RaycastJob
                {
                    RaycastHits = raycastHits,
                    DeltaTime = Time.deltaTime,
                    World = world,
                    Entities = entities,
                    Translations = translations,
                    MovementStats = movementStats,
                    Rotations = rotations,
                    Targets = targets,
                };

                var handle = new JobHandle();
                job.Run(MovingEntityQuery.CalculateEntityCount());

                handle.Complete();

                raycastHits.Dispose();
                entities.Dispose();
                translations.Dispose();
                movementStats.Dispose();
                rotations.Dispose();
                targets.Dispose();
            }
        }

        [BurstCompatible]
        public struct RaycastJob : IJobFor
        {
            public NativeList<RaycastHit> RaycastHits;

            [ReadOnly] public float DeltaTime;
            [ReadOnly] public PhysicsWorld World;
            [ReadOnly] public NativeArray<Entity> Entities;
            [ReadOnly] public NativeArray<Translation> Translations;
            [ReadOnly] public NativeArray<MovementStats> MovementStats;
            [ReadOnly] public NativeArray<Rotation> Rotations;
            [ReadOnly] public NativeArray<Target> Targets;

            public void Execute(int i)
            {
                RaycastHits.Clear();

                var raycastInput = new RaycastInput
                {
                    Start = Translations[i].Value,
                    End = Targets[i].Position,
                    Filter = CollisionFilter.Default
                };

                var hasHits = World.CastRay(raycastInput, ref RaycastHits);

                if (hasHits)
                {
                    foreach (var hit in RaycastHits)
                    {
                        if (hit.Entity != Targets[i].Entity) continue;

                        if (math.any(math.abs(hit.Position - Translations[i].Value) > MovementStats[i].EngageRange))
                        {
                            var newPosition = Translations[i].Value + math.forward(Rotations[i].Value) * MovementStats[i].MoveSpeed * DeltaTime;
                            // This assumes that the units will always move along the same y. If we decide to include
                            //  elevations, this will need to change. Changing it to physics-based movment could work,
                            //  though would likely be more computationally heavy
                            newPosition.y = 0.0F;
                            EntityManager.SetComponentData(Entities[i], new Translation { Value = newPosition });
                        }
                    }
                }
            }
        }
    }
}
