using System;
using System.Collections;
using Components;
using Components.AuthoringTags;
using JetBrains.Annotations;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine.InputSystem;

namespace MonoBehaviours.Input
{
    public partial class PlayerController
    {
        private static CollisionWorld CollisionWorld => PhysicsWorld.CollisionWorld;

        private static EntityQuery MultiSelectorQuery =>
            EntityManager.CreateEntityQuery(typeof(MultiSelector));

        private bool drawingMultiSelect;

        [UsedImplicitly]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        private IEnumerator OnSingleSelect(InputValue input)
        {
            cleanPreviousSelections();
            cleanPreviousTargets();

            if (pointerPositionIsSelectable(out var hit))
            {
                drawingMultiSelect = true;

                var clickPos = hit.Position;
                var originPoint = new float3(clickPos.x, 0.0F, clickPos.z);

                Instantiate(multiSelectPrefab);

                // Yield here to allow for the EntityManager to actually create the entity during the next frame
                yield return null;

                var multiSelectors = MultiSelectorQuery.ToEntityArray(Allocator.Temp);
                if (multiSelectors.Length < 1) throw new InvalidOperationException("No multi-selector was found");

                var selector = multiSelectors[0];
                EntityManager.SetComponentData(selector, new MultiSelector { OriginPoint = originPoint });
                EntityManager.SetComponentData(selector, new Translation { Value = originPoint });
            }
        }

        private static void cleanPreviousSelections()
        {
            var prevSelections = SelectedEntityQuery.ToEntityArray(Allocator.Temp);
            foreach (var selection in prevSelections)
            {
                EntityManager.RemoveComponent<Selected>(selection);
            }

            prevSelections.Dispose();
        }

        [UsedImplicitly]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        private IEnumerator OnHandleSelections()
        {
            if (drawingMultiSelect)
            {
                var multiSelectors = MultiSelectorQuery.ToEntityArray(Allocator.Temp);
                if (multiSelectors.Length > 0)
                {
                    foreach (var selector in multiSelectors)
                    {
                        // TODO: On occasion, this fails to select if the button is somehow pressed and released
                        //  in the span of the same frame. The obvious solution is to just yield until the next
                        //  frame, but the temp allocation of multiSelectors means that the resources are deallocated
                        //  before that. For the most part this shouldn't be a huge window of opportunity, but
                        //  on weaker hardware or if calculations are just taking oddly long, this will fail.
                        //  This could possibly be corrected by converting this to a job and using a TempJob allocator.
                        if (!EntityManager.HasComponent<WorldRenderBounds>(selector)) yield return null;

                        var renderBounds = EntityManager.GetComponentData<WorldRenderBounds>(selector);
                        var physicsBounds = new Aabb
                        {
                            Max = renderBounds.Value.Max,
                            Min = renderBounds.Value.Min
                        };

                        var allHits = new NativeList<int>(Allocator.Temp);
                        if (CollisionWorld.OverlapAabb(
                            new OverlapAabbInput
                            {
                                Aabb = physicsBounds,
                                Filter = targetableLayerFilter
                            },
                            ref allHits
                        ))
                        {
                            // ReSharper disable once ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
                            foreach (var hit in allHits)
                            {
                                var selectedEntity = PhysicsWorld.Bodies[hit].Entity;
                                // TODO: This check won't be necessary once the filter is properly applied
                                if (EntityManager.HasComponent<PlayerUnit>(selectedEntity))
                                    EntityManager.AddComponent<Selected>(selectedEntity);
                            }
                        }

                        EntityManager.DestroyEntity(selector);
                    }
                }

                drawingMultiSelect = false;
            }
        }
    }
}
