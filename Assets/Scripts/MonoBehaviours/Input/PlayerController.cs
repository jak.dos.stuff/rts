using System;
using Components;
using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;
using UnityEngine.InputSystem;
using RaycastHit = Unity.Physics.RaycastHit;

namespace MonoBehaviours.Input
{
    public partial class PlayerController : MonoBehaviour
    {
        private static Camera MainCamera => Camera.main;
        private static EntityManager EntityManager => World.DefaultGameObjectInjectionWorld.EntityManager;
        private static EntityQuery TargetedEntityQuery => EntityManager.CreateEntityQuery(typeof(Targeted));
        private static EntityQuery SelectedEntityQuery => EntityManager.CreateEntityQuery(typeof(Selected));

        private static PhysicsWorld PhysicsWorld =>
            World.DefaultGameObjectInjectionWorld.GetExistingSystem<BuildPhysicsWorld>().PhysicsWorld;

        private static uint selectableLayerMask;
        private static CollisionFilter selectableLayerFilter;

        private static uint targetableLayerMask;
        private static CollisionFilter targetableLayerFilter;

        public GameObject multiSelectPrefab;

        private void Start()
        {
            if (MainCamera is null) throw new MissingMemberException("No camera found");
            
            selectableLayerMask = (uint)LayerMask.GetMask("Ground");
            selectableLayerFilter = new CollisionFilter()
            {
                BelongsTo = selectableLayerMask,
                CollidesWith = selectableLayerMask
            };

            targetableLayerMask = (uint)LayerMask.GetMask("Unit");
            targetableLayerFilter = new CollisionFilter()
            {
                BelongsTo = targetableLayerMask,
                CollidesWith = targetableLayerMask
            };
        }

        private static bool pointerPositionIsTargetable(out RaycastHit hit)
        {
            return isObjectAtpointerPosition(targetableLayerFilter, out hit);
        }

        private static bool pointerPositionIsSelectable(out RaycastHit hit)
        {
            return isObjectAtpointerPosition(selectableLayerFilter, out hit);
        }

        private static bool isObjectAtpointerPosition(CollisionFilter collisionFilter, out RaycastHit hit)
        {
            var pointerPosition = Pointer.current.position.ReadValue();
            var positionRay = MainCamera.ScreenPointToRay(pointerPosition);

            var raycastInput = new RaycastInput
            {
                Start = positionRay.origin,
                End = positionRay.GetPoint(1000.0F),
                Filter = collisionFilter
            };

            return PhysicsWorld.CastRay(raycastInput, out hit);
        }
    }
}
