using System.Linq;
using Components;
using Components.AuthoringTags;
using JetBrains.Annotations;
using Unity.Collections;
using Unity.Transforms;

namespace MonoBehaviours.Input
{
    public partial class PlayerController
    {
        [UsedImplicitly]
        private void OnTarget()
        {
            cleanPreviousTargets();

            var selectedEntities = SelectedEntityQuery.ToEntityArray(Allocator.Temp);
            if (selectedEntities.Length == 0) return;
            if (selectedEntities.Any(entity => EntityManager.HasComponent<EnemyUnit>(entity))) return;

            if (!pointerPositionIsTargetable(out var hit)) return;

            var targetedEntity = PhysicsWorld.Bodies[hit.RigidBodyIndex].Entity;

            // TODO: This check will need to change if we want to implement a "move here" target
            if (EntityManager.HasComponent<Translation>(targetedEntity))
            {
                EntityManager.AddComponent<Targeted>(targetedEntity);
            }
        }

        private static void cleanPreviousTargets()
        {
            // There should only ever be a single target at a time, but ToEntityArray returns a NativeArray
            var previousTargets = TargetedEntityQuery.ToEntityArray(Allocator.Temp);
            foreach (var target in previousTargets)
            {
                EntityManager.RemoveComponent<Targeted>(target);
            }

            previousTargets.Dispose();
        }
    }
}
